package com.example.romanconverter;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import org.w3c.dom.Text;

public class MainActivity extends AppCompatActivity {

    Button btn_ConvertToRoman, btn_ConvertToNumber;
    EditText et_number, et_roman;
    TextView tv_RomanOutput, tv_NumberOutput;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        btn_ConvertToRoman = (Button) findViewById(R.id.btn_ConvertToRoman);
        btn_ConvertToNumber = (Button) findViewById(R.id.btn_ConvertToNumber);

        et_number = (EditText) findViewById(R.id.et_number);
        et_roman = (EditText) findViewById(R.id.et_roman);

        tv_NumberOutput = (TextView) findViewById(R.id.tv_NumberOutput);
        tv_RomanOutput = (TextView) findViewById(R.id.tv_romanOut);


        btn_ConvertToRoman.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
            //create new NumberConverter to use convert to roman method
            NumberConverter nc = new NumberConverter();

            //create int that will store user input
            int theNumber; //the input from the user

                //create string that will store the answer and be used to display to user
                String theRoman; //string to send back to the user

            //store user input into int
            theNumber = Integer.parseInt(et_number.getText().toString());


            //store entered number into string for display by using int that holds user input
            theRoman = nc.toRoman(theNumber);

            //display to user using text view RomanOutput
            tv_RomanOutput.setText(theRoman);


            }
        });

        btn_ConvertToNumber.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                NumberConverter nc2 = new NumberConverter();

                String userInput;

                int conversion;

                userInput = (et_roman.getText().toString());

                conversion = nc2.toNumber(userInput);


                tv_NumberOutput.setText( String.valueOf(conversion));


            }
        });
    }
}
