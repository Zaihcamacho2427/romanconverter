package com.example.romanconverter;

public class NumberConverter {

    public String toRoman(int numberInput){

        if (numberInput < 0 || numberInput > 10000){
            return "Sorry. I can't do that";
        }


        String returnValue = "";


        while(numberInput >= 1000){
            returnValue += "M";
            numberInput = numberInput - 1000;
        }


        while(numberInput >= 900){
            returnValue += "CM";
            numberInput = numberInput - 900;
        }


        while(numberInput >= 500){
            returnValue += "D";
            numberInput = numberInput - 500;
        }


        while(numberInput >= 400){
            returnValue += "CD";
            numberInput = numberInput - 400;
        }



        while(numberInput >= 100){
            returnValue += "C";
            numberInput = numberInput - 100;
        }



        while(numberInput >= 90){
            returnValue += "XC";
            numberInput = numberInput - 90;
        }



        while(numberInput >= 50){
            returnValue += "L";
            numberInput = numberInput - 50;
        }


        while(numberInput >= 40){
            returnValue += "XL";
            numberInput = numberInput - 40;
        }


        while(numberInput >= 10){
            returnValue += "X";
            numberInput = numberInput - 10;
        }


        while(numberInput >= 9){
            returnValue += "IX";
            numberInput = numberInput - 9;
        }


        while(numberInput >= 5){
            returnValue += "V";
            numberInput = numberInput - 5;
        }


        while(numberInput >= 4){
            returnValue += "IV";
            numberInput = numberInput - 4;
        }


        while(numberInput >= 1){
            returnValue += "I";
            numberInput = numberInput - 1;
        }

        //return a string value of numeral
        return returnValue;
    }


    public int toNumber (String romanInput){
        //int that will store value of conversion
        int decimal = 0;
        //create string that will be the uppercase version of entered roman numeral
        String romanNumeral = romanInput.toUpperCase();
        //for loop going through user input
        for(int x = 0; x<romanNumeral.length();x++)
        {
            //character that looks through each roman numeral entered and used for switch statement
            char convertToDecimal = romanNumeral.charAt(x);

            switch (convertToDecimal)
            {
                case 'M':
                    decimal += 1000;
                    break;

                case 'D':
                    decimal += 500;
                    break;

                case 'C':
                    decimal += 100;
                    break;

                case 'L':
                    decimal += 50;
                    break;

                case 'X':
                    decimal += 10;
                    break;

                case 'V':
                    decimal += 5;
                    break;

                case 'I':
                    decimal += 1;
                    break;
            }
        }

        //if user input contains any roman numeral conversion that uses subtraction, edit decimal value as needed.
        if (romanNumeral.contains("IV"))
        {
            decimal-=2;
        }
        if (romanNumeral.contains("IX"))
        {
            decimal-=2;
        }
        if (romanNumeral.contains("XL"))
        {
            decimal-=20;
        }
        if (romanNumeral.contains("XC"))
        {
            decimal-=20;
        }
        if (romanNumeral.contains("CD"))
        {
            decimal-=200;
        }
        if (romanNumeral.contains("CM"))
        {
            decimal-=200;
        }



        return decimal;
    }
}
